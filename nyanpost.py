#!/usr/bin/env python3
import requests
import sys
import json

UPLOAD_URL = 'https://desu.sh/upload.php'
DOWNLOAD_URL = 'https://a.desu.sh/'


def main():
    file_names = sys.argv[1:]
    if len(file_names) == 0:
        print('I didn\'t understand you, onii-chan~')
        print('Use me like: {} file1 [file2 [file3 ... ]]'.format(sys.argv[0]))
        exit(0)
    files = [('files[]', open(file_name, 'rb')) for file_name in file_names]
    raw_response = requests.post(UPLOAD_URL, files=files).content.decode()
    try:
        response = json.loads(raw_response)
    except json.decoder.JSONDecodeError as exc:
        print('Server returned something unreadable~')
        print('He said:', raw_response)
        exit(1)
    if not response['success']:
        print('I can\'t do what you want, sorry~')
        print('Server said:')
        print(json.dumps(response, indent=4))
        exit(1)
    if 'files' in response:
        print('I\'ve posted your data to server, nyan ^_^. You can find:')
        for file_obj in response['files']:
            print('"{}" at {} (hash {})'.format(file_obj['name'],
                                                DOWNLOAD_URL + file_obj['url'],
                                                file_obj['hash']))


if __name__ == '__main__':
    main()
